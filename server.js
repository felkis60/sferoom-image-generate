const http = require('http');
const uuid4 = require('uuid4');
const express = require('express');
const request = require('request');

(async () => {
  const app = express();
  const port = 49161;

  app.set('port', port);
  app.set('trust proxy', true)
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  const server = http.Server(app);

  server.listen(app.get('port'), () => {
    console.log('server listen on ' + app.get('port'));

    app.get('/v1/image-generate', async (clientReq, res) => {
      console.info(`[INFO] ${clientReq.path} from ${clientReq.ip}`);
      if (clientReq.method === 'GET' && clientReq.query.prompt && clientReq.query.style) {
        const styles = {
          'ca': 'Concept art',
          'pd': 'Pencil drawing',
          'op': 'Oil painting',
          'r': 'Realistic'
        }
        try {
          const translatedText = await translate(clientReq.query.prompt);
          console.info(`[INFO] translatedText: ${translatedText}`);
          console.info(`[INFO] styles: ${styles[clientReq.query.style]}`);
          const { images } = await generateAsync({
            prompt: `${translatedText} ${styles[clientReq.query.style]}`,
          })

          const authRes = await selTokens();
          if (authRes.statusCode === 200) {
            const token = authRes.body.access.token.id;
            const endpoint = authRes.body.access.serviceCatalog[0].endpoints[0].adminURL;
            const filename = uuid4();
            const url = await uploadFile({
              endpoint, token, image: images[0], filename
            })
            console.info(`[INFO] url: https://363427.selcdn.ru/sferoom/dream/${filename}.png`);
            res.send({
              payload: {
                url: `https://363427.selcdn.ru/sferoom/dream/${filename}.png`
              }
            })
          }
        } catch (e) {
          res.send({ err: e.message })
          console.error(`[ERROR] ${e.message}`);
        }
      }
    });
  });
})();

function generateAsync ({ prompt }) {
  return new Promise((resolve, reject) => {
    request(
      {
        url: "https://api.stability.ai/v1/generation/stable-diffusion-768-v2-1/text-to-image",
        method: "POST",
        headers: {
          'Authorization': 'sk-BZQE1LUBKZAVDIRg78yOD86ep0VgC30eFmaIB7lnpb4a3SVb',
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        json: {
          cfg_scale: 15,
          height: 1024,
          width: 1024,
          text_prompts: [{
            text: prompt,
            weight: 1,
          }],
        }
      }, function (err, res) {
        console.info(`[INFO] translatedText: `, {
          body: res.body,
        });
        if (err) reject(err)
        else resolve({
          images: res.body.artifacts.map(item => ({
            buffer: Buffer.from(item.base64, 'base64')
          }))
        })
      }
    )
  })
}

function selTokens () {
  return new Promise((resolve, reject) => {
    request(
      {
        url: "https://api.selcdn.ru/v2.0/tokens",
        method: "POST",
        json: {
          'auth': {
            'passwordCredentials': {
              'username': '105539',
              'password': 'dSb5VT8C',
            }
          }
        },
      }, function (authError, authRes) {
        if (authError) reject(authError)
        else resolve(authRes)
      }
    )
  })
}

function translate (text) {
  return new Promise((resolve, reject) => {
    request(
      {
        url: "http://prod.nord.fondo.ru:6326/v1/translate",
        method: "POST",
        json: {
          "text": text
        },
      }, function (translateError, translateRes) {
        if (translateError) reject(translateError)
        else resolve(translateRes.body.payload.result)
      }
    )
  })
}

function uploadFile ({ endpoint, token, image, filename }) {
  return new Promise((resolve, reject) => {
    request(
      {
        url: `${endpoint}/sferoom/dream/${filename}.png`,
        method: "PUT",
        headers: {
          'X-Auth-Token': token,
          'Content-Type': 'application/octet-stream'
        },
        body: image.buffer
      }, function (imageError, imageRes) {
        if (imageError) reject(imageError)
        else resolve(`https://363427.selcdn.ru/sferoom/dream/${filename}.png`)
      }
    )
  })
}